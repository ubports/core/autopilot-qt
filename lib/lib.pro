TEMPLATE = lib
TARGET = qttestability
DESTDIR=..

# disable qt includes and linkage (core and gui are enabled per default with Qt4)
QT -= core gui

QMAKE_CXXFLAGS += -std=c++0x -Wl,--no-undefined
QMAKE_CXXFLAGS -= -pedantic
QMAKE_LFLAGS += -Wl,--no-undefined

LIBS += -ldl

SOURCES = qttestability.cpp
HEADERS = qttestability.h

target.file = libqttestability*
target.path = $$[QT_INSTALL_LIBS]

INSTALLS += target
