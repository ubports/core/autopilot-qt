Source: autopilot-qt
Priority: optional
Maintainer: UBports developers <devs@ubports.com>
Build-Depends: debhelper-compat (= 12),
               libgl1-mesa-dev,
               libgles2-mesa-dev,
               libxpathselect-dev (>= 1.4),
               mesa-common-dev,
               pkg-config,
               qt5-qmake,
               qtbase5-dev,
               qtchooser,
               qtdeclarative5-dev,
               xvfb,
Standards-Version: 3.9.4
Section: libs
Homepage: https://gitlab.com/ubports/core/autopilot-qt
Vcs-Git: https://gitlab.com/ubports/core/autopilot-qt.git
Vcs-Browser: https://gitlab.com/ubports/core/autopilot-qt

Package: libautopilot-qt
Architecture: all
Depends: autopilot-qt5,
         qttestability-autopilot,
         ${misc:Depends},
         ${shlibs:Depends},
Description: make Qt applications introspectable by autopilot
 autopilot-qt allows autopilot to test any existing Qt application, without
 having to rebuild the application under test.
 .
 This is a compatibility package depending on the Qt5 drivers.

Package: autopilot-qt5
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Replaces: libautopilot-qt (<< 1.4+14.10.20140724.1-0ubuntu2)
Breaks: libautopilot-qt (<< 1.4+14.10.20140724.1-0ubuntu2)
Description: make Qt applications introspectable by autopilot - Qt5 plugin
 autopilot-qt allows autopilot to test any existing Qt application, without
 having to rebuild the application under test.
 .
 This package provides the driver used for packages built against Qt5.

Package: qttestability-autopilot
Architecture: any
Multi-Arch: same
Depends: python3-autopilot (>= 1.4),
         autopilot-qt5 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Replaces: libautopilot-qt (<< 1.4+14.10.20140724.1-0ubuntu2)
Breaks: libautopilot-qt (<< 1.4+14.10.20140724.1-0ubuntu2)
Description: make Qt applications introspectable by autopilot
 autopilot-qt allows autopilot to test any existing Qt application, without
 having to rebuild the application under test.
 .
 This package provides the qttestability library, which requires the
 autopilot-qt5 driver package installed for the application under test.

Package: libautopilot-qt-autopilot
Architecture: any
Depends: autopilot-qt5 (= ${binary:Version}),
         python3:any,
         python3-autopilot,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Autopilot tests for libautopilot-qt
 This contains sample Qt5 apps and autopilot tests to test libautopilot-qt.
