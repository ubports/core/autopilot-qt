TEMPLATE = app

TARGET = qt5testapp
QT += widgets quick
qmlfile.file = qt5.qml

SOURCES += testapp.cpp

qmlfile.path=/usr/share/libautopilot-qt/

target.path=$$[QT_INSTALL_BINS]
target.file = $TARGET

INSTALLS += target qmlfile 

OTHER_FILES += \
    $$system(ls ./*.qml)
